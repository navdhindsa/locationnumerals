# LocationNumerals - conversions for any value

This class in Python converts from decimal numbers to location numerals and back.

It has 3 methods:
One method that takes an integer and returns the location numeral in abbreviated form. For example, we pass in 9 and it returns “ad"
One method that takes a location numeral and returns its value as an integer. For example, we pass “ad” in, and it returns 9
One method that takes a location numeral and returns it in abbreviated form. For example, we pass in “abbc” and it returns “ad"

