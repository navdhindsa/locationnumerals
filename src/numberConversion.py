#This class contains 3 methods:
#1. to convert location numerals to decimal
#2. to convert decimal numbers to location numerals
#3. to abbreviate the location numerals
#Note: There is no limit on the numbers, they can be greater than 2^27 i.e. 134217728

#The Array of Location Numeral letters
val = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

#The class numbers
class Numbers:
    #Function to convert location numerals to decimal    
    def locToDec(self, input_num):
        
        res=0
        #Iterating through location numeral and adding it to the result in decimal format and printing the result
        for x in input_num:
            res= res + (2**val.index(x))
        print (res)
    
    #Function to convert decimal numbers to location numerals  
    def decToLoc(self, input_num):
        
        res =''
        
        #solution for numbers greater than 2^27, add a 'z' to result and subtract from number
        while input_num > 134217728:
            res = res + 'z'
            input_num = input_num-134217728

        #convert number to binary using inbuilt function  
        filt = bin(input_num)[2:]
        
        i=0

        #Converting the binary number to location numeral
        for x in reversed(filt):
            #
            if x=='1':
                res= res +( val[i])
            i=i+1
        print(res)
           
    #Abbreviating the location numeral       
    def locAbbr(self,input_num):
        #Variables
        dec =0
        res =''
        
        #if the numeral has z it can not be simplified further, otherwise convert the remaining numerals to decimal to resolve
        for i in input_num: 
            if i == 'z':
                res = res + 'z'
            else:
                dec = dec + 2**val.index(i)

        #In case the resulting number is out of range
        while dec > 134217728:
            res = res + 'z'
            dec = dec-134217728

        #binary version of the numbers    
        filt = bin(dec)[2:]
        
        i=0
        #converting binary to location numeral in abbreviated form
        for x in reversed(filt): 
            if x=='1':
                res= res +( val[i])
            i=i+1
        print(res)
#    
def main():
    nums = Numbers()
    #change the values to be passed here
    nums.locToDec('ad')
    nums.decToLoc(9)
    nums.locAbbr('abbc')
if __name__ == '__main__':
    main()